package desafio.stoom.petz;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("desafio.stoom.petz");

        noClasses()
            .that()
            .resideInAnyPackage("desafio.stoom.petz.service..")
            .or()
            .resideInAnyPackage("desafio.stoom.petz.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..desafio.stoom.petz.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
