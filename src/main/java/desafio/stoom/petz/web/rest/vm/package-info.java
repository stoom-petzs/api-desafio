/**
 * View Models used by Spring MVC REST controllers.
 */
package desafio.stoom.petz.web.rest.vm;
