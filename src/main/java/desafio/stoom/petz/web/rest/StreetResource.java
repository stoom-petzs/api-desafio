package desafio.stoom.petz.web.rest;

import desafio.stoom.petz.domain.Street;
import desafio.stoom.petz.repository.StreetRepository;
import desafio.stoom.petz.service.StreetService;
import desafio.stoom.petz.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link desafio.stoom.petz.domain.Street}.
 */
@RestController
@RequestMapping("/api")
public class StreetResource {

    private final Logger log = LoggerFactory.getLogger(StreetResource.class);

    private static final String ENTITY_NAME = "street";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StreetService streetService;

    private final StreetRepository streetRepository;

    public StreetResource(StreetService streetService, StreetRepository streetRepository) {
        this.streetService = streetService;
        this.streetRepository = streetRepository;
    }

    /**
     * {@code POST  /streets} : Create a new street.
     *
     * @param street the street to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new street, or with status {@code 400 (Bad Request)} if the street has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/streets")
    public ResponseEntity<Street> createStreet(@Valid @RequestBody Street street) throws URISyntaxException {
        log.debug("REST request to save Street : {}", street);
        if (street.getId() != null) {
            throw new BadRequestAlertException("A new street cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Street result = streetService.save(street);
        return ResponseEntity
            .created(new URI("/api/streets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /streets/:id} : Updates an existing street.
     *
     * @param id the id of the street to save.
     * @param street the street to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated street,
     * or with status {@code 400 (Bad Request)} if the street is not valid,
     * or with status {@code 500 (Internal Server Error)} if the street couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/streets/{id}")
    public ResponseEntity<Street> updateStreet(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody Street street
    ) throws URISyntaxException {
        log.debug("REST request to update Street : {}, {}", id, street);
        if (street.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, street.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!streetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Street result = streetService.save(street);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, street.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /streets/:id} : Partial updates given fields of an existing street, field will ignore if it is null
     *
     * @param id the id of the street to save.
     * @param street the street to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated street,
     * or with status {@code 400 (Bad Request)} if the street is not valid,
     * or with status {@code 404 (Not Found)} if the street is not found,
     * or with status {@code 500 (Internal Server Error)} if the street couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/streets/{id}", consumes = "application/merge-patch+json")
    public ResponseEntity<Street> partialUpdateStreet(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody Street street
    ) throws URISyntaxException {
        log.debug("REST request to partial update Street partially : {}, {}", id, street);
        if (street.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, street.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!streetRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<Street> result = streetService.partialUpdate(street);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, street.getId().toString())
        );
    }

    /**
     * {@code GET  /streets} : get all the streets.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of streets in body.
     */
    @GetMapping("/streets")
    public List<Street> getAllStreets() {
        log.debug("REST request to get all Streets");
        return streetService.findAll();
    }

    /**
     * {@code GET  /streets/:id} : get the "id" street.
     *
     * @param id the id of the street to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the street, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/streets/{id}")
    public ResponseEntity<Street> getStreet(@PathVariable Long id) {
        log.debug("REST request to get Street : {}", id);
        Optional<Street> street = streetService.findOne(id);
        return ResponseUtil.wrapOrNotFound(street);
    }

    /**
     * {@code DELETE  /streets/:id} : delete the "id" street.
     *
     * @param id the id of the street to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/streets/{id}")
    public ResponseEntity<Void> deleteStreet(@PathVariable Long id) {
        log.debug("REST request to delete Street : {}", id);
        streetService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString()))
            .build();
    }
}
