package desafio.stoom.petz.service;

import desafio.stoom.petz.common.Geocode;
import desafio.stoom.petz.common.Location;
import desafio.stoom.petz.domain.Street;
import desafio.stoom.petz.repository.StreetRepository;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

/**
 * Service Implementation for managing {@link Street}.
 */
@Service
@Transactional
public class StreetService {

//    @Value("${uri.acess.geolocalizacao}")
//    private String geolocalizacao;
//
//    @Value("${uri.acess.key}")
//    private String keyGeoLocalizacao;

    private final Logger log = LoggerFactory.getLogger(StreetService.class);

    private final StreetRepository streetRepository;

    public StreetService(StreetRepository streetRepository) {
        this.streetRepository = streetRepository;
    }

    /**
     * Save a street.
     *
     * @param street the entity to save.
     * @return the persisted entity.
     */
    public Street save(Street street) {
        if (Objects.isNull(street.getLatitude()) || Objects.isNull(street.getLongitude()) ||
            street.getLatitude() == 0 || street.getLongitude() == 0) {
            Location localizacao =  getGeoloacationGMaps(street);
            street.setLatitude(localizacao.getLat());
            street.setLongitude(localizacao.getLng());
            return streetRepository.save(street);
        }else {
            return streetRepository.save(street);
        }
    }

    public  Location getGeoloacationGMaps(Street street) {
        try {
            String url = String.format("https://maps.google.com/maps/api/geocode/json?address=%s,%s,%s,%s&components=country:BR&key=AIzaSyBfXJlkfqCcXM6bCQNgUGLcIo3x7uVoXFk" ,
                street.getZipcode(),
                street.getStreetName(),
                street.getNumber(),
                street.getCountry()
            ) ;
            RestTemplate rt = new RestTemplate();
            Geocode geocode = rt.getForObject(url, Geocode.class);
            return geocode.getResults().get(0).getGeometry().getLocation();
        } catch (HttpClientErrorException e) {
            throw new HttpClientErrorException(e.getStatusCode(), e.getMessage());
        }
    }


    /**
     * Partially update a street.
     *
     * @param street the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<Street> partialUpdate(Street street) {
        log.debug("Request to partially update Street : {}", street);

        return streetRepository
            .findById(street.getId())
            .map(
                existingStreet -> {
                    if (street.getStreetName() != null) {
                        existingStreet.setStreetName(street.getStreetName());
                    }
                    if (street.getNumber() != null) {
                        existingStreet.setNumber(street.getNumber());
                    }
                    if (street.getComplement() != null) {
                        existingStreet.setComplement(street.getComplement());
                    }
                    if (street.getNeighbourhood() != null) {
                        existingStreet.setNeighbourhood(street.getNeighbourhood());
                    }
                    if (street.getCity() != null) {
                        existingStreet.setCity(street.getCity());
                    }
                    if (street.getState() != null) {
                        existingStreet.setState(street.getState());
                    }
                    if (street.getCountry() != null) {
                        existingStreet.setCountry(street.getCountry());
                    }
                    if (street.getZipcode() != null) {
                        existingStreet.setZipcode(street.getZipcode());
                    }
                    if (street.getLatitude() != null) {
                        existingStreet.setLatitude(street.getLatitude());
                    }
                    if (street.getLongitude() != null) {
                        existingStreet.setLongitude(street.getLongitude());
                    }

                    return existingStreet;
                }
            )
            .map(streetRepository::save);
    }

    /**
     * Get all the streets.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<Street> findAll() {
        log.debug("Request to get all Streets");
        return streetRepository.findAll();
    }

    /**
     * Get one street by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Street> findOne(Long id) {
        log.debug("Request to get Street : {}", id);
        return streetRepository.findById(id);
    }

    /**
     * Delete the street by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Street : {}", id);
        streetRepository.deleteById(id);
    }
}
